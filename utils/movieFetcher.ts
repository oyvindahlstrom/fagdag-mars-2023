export interface IMovie {
    id: number
    adult: boolean
    backdrop_path: string | null
    genre_ids: number[]
    original_language: string
    original_title: string
    overview: string
    popularity: number
    poster_path: string | null
    release_date: string
    title: string
    video: boolean
    vote_average: number
    vote_count: number
}

export const imageBasePath = "https://image.tmdb.org/t/p/original/"

export async function getMovies() {
    const data = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}`);
    const res: { results: IMovie[], page: number, total_results: number, total_pages: number } = await data.json();
    return res;
}

export async function getMovie(id: number) {
    const data = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.API_KEY}`, {
        next: {revalidate: 0}
    });
    const res = await data.json();
    return res;
}