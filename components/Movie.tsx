"use client"

import {IMovie} from "../utils/movieFetcher";
import Link from "next/link";
import Image from "next/image";
// import {useRouter} from "next/router";

export default function Movie(movie: IMovie) {
    //const router = useRouter();
    const imagePath = "https://image.tmdb.org/t/p/original/"
    return (
        <div>
            <h1 className={'text-center h-12'}>{movie.title}</h1>
            <Link href={`/${movie.id}`}>
                <Image
                    className={'rounded-md'}
                    src={imagePath + movie.poster_path}
                    alt={movie.title}
                    width={800}
                    height={800}
                />
            </Link>
            <div className={'text-sm'}>
                <h2>Released: {movie.release_date}</h2>
                <h2>Rating: {movie.vote_average} / 10</h2>
            </div>
        </div>
    )
}