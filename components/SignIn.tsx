"use client"

import {signIn} from "next-auth/react";

export function SignIn() {
    return (
        <button className={'mb-4 flex rounded-md border border-gray-800 bg-green-600 px-4 py-3 text-sm'} onClick={() => signIn('google')}>
            Sign in
        </button>
    )
}