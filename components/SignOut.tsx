"use client"
import {signOut, useSession} from "next-auth/react";
import Image from "next/image";

export function SignOut() {
    const {data: session} = useSession()
    return (
        <>
            <div className={'flex items-center'}>
                {session?.user?.image &&
                <Image src={session?.user?.image || ''} alt={session?.user?.name || 'User Icon'} height={40} width={40} className={'rounded-full'}/>
                }
                <h2 className={'ml-2'}>Hi, {session?.user?.name}!</h2>
            </div>
            <button
                className={'mt-2 mb-6 text-xs text-white hover:text-[hsl(280,100%,70%)]'}
                onClick={() => signOut()}
            >
                Sign Out
            </button>
        </>
    )
}