import React from 'react';

export const metadata = {
    title: "Progit fagdag 2023",
    description: "A description",
    openGraph: {title: "Progit Fagdag 2023"},
    robots: {index: true}
}

function AboutLayout({children}: React.FC) {
    return (
        <section>
            <ul>
                <li>Hello</li>
                <li>Path</li>
            </ul>
            {children}
        </section>
    )
}

export default AboutLayout;
