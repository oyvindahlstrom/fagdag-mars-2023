"use client"
import React, {useEffect} from 'react';

interface Props {
    error: Error,
    reset: () => void
}

export default function Error({error, reset}: Props) {

    useEffect(() => console.error(error), [error]);

    return (
        <>
            <h2 style={{color: 'red'}}>Noe gikk galt: {error.message}</h2>
            <button onClick={reset}>Forsøk på nytt</button>
        </>
    );
}
