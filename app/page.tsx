import Movie from "../components/Movie";
import {IMovie} from "../utils/movieFetcher";

export default async function Home() {
  const data = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}`)
  const res: { results: IMovie[], page: number, total_results: number, total_pages: number } = await data.json()

  return (
      <div className={'grid grid-cols-fluid gap-16'}>
        {res.results.map((movie) => <Movie key={movie.id} {...movie} />)}
      </div>
  )
}
