import Image from "next/image";
import {getMovie, imageBasePath} from "@/utils/movieFetcher";

export default async function MovieDetail({params}: { params: { movieId: number } }) {
    const movie = await getMovie(params.movieId)
    return (
        <div>
            <div className={'relative'}>
                <div className={'absolute bottom-0 p-4 bg-opacity-60 rounded-r-md bg-black'}>
                    <h2 className={'text-2xl'}>{movie.title}</h2>
                    <h2 className={'text-lg'}>{movie.release_date}</h2>
                    <h2>Runtime: {movie.runtime} minutes</h2>
                    <h2 className={'bg-green-600 inline-block my-2 py-1 px-2 rounded-md text-xs'}>{movie.status}</h2>
                </div>
                <Image
                    src={imageBasePath + movie.backdrop_path}
                    alt={movie.title}
                    className={'my-12 w-full rounded-xl'}
                    width={1000}
                    height={1000}
                    priority
                />
            </div>
            <p>{movie.overview}</p>
        </div>
    )
}