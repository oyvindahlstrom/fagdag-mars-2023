import Link from "next/link";
import {ReactNode} from "react";

export default function MovieLayout({children}: { children: ReactNode }) {
    return (
        <article>
            <div>
                <Link className={'hover:underline hover:cursor-pointer'} href={'/'}>
                    Back
                </Link>
            </div>
            {children}
        </article>
    )
}